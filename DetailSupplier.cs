﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TugasBesarVisualDesktop
{
    public partial class DetailSupplier : Form
    {
        string id;
        string nama;
        string alamat;
        DaftarSupplier ds;
        public DetailSupplier()
        {
            InitializeComponent();
        }

        public void initData(string id, string nama, string alamat)
        {
            this.id = id;
            this.nama = nama;
            this.alamat = alamat;
            tbNama.Text = nama;
            tbAlamat.Text = alamat;
            ds = (DaftarSupplier)Application.OpenForms["DaftarSupplier"];
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string namabaru = tbNama.Text;
            string alamatbaru = tbAlamat.Text;
            string sql = "update supplier set namasupplier = '"+namabaru+"', alamatsupplier = '"+alamatbaru+"' where id = "+id;
            int x = Connection.executeNonQuery(sql);
            if(x > 0)
            {
                MessageBox.Show("Update Berhasil");
                ds.loadData();
                ds.dataGridView1.Update();
                ds.dataGridView1.Refresh();
                this.Close();
            } else
            {
                MessageBox.Show("Gagal Update");
            }

        }
    }
}
