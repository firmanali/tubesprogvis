﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace TugasBesarVisualDesktop
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string username = tbUsername.Text;
            string password = tbPassword.Text;
            if(login(username, password))
            {
                MainMenu mm = new MainMenu();
                this.Close();
                mm.Show();
            }
            
        }

        private Boolean login(string username, string password)
        {
            Boolean res = false;
            string sql = "select id, password from users where username = '"+username+"'";
            SqlConnection conn = Connection.koneksi();
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = sql;
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    string passDb = String.Format("{0}",reader[1]);
                    if (passDb.Equals(password))
                    {
                        res = true;
                        Values.userId = reader.GetInt32(0);
                        Values.username = username;
                    }
                }
            }
            reader.Close();
            return res;
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }
    }
}
