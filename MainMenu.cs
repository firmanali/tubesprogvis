﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TugasBesarVisualDesktop
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void MainMenu_Load(object sender, EventArgs e)
        {

        }

        private void buatProdukToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuatProduk bp = new BuatProduk();
            bp.MdiParent = this;
            bp.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void buatSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuatSupplier bs = new BuatSupplier();
            bs.MdiParent = this;
            bs.Show();
        }

        private void daftarSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DaftarSupplier ds = new DaftarSupplier();
            ds.MdiParent = this;
            ds.Show();
        }

        private void daftarProdukToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DaftarProduk dp = new DaftarProduk();
            dp.MdiParent = this;
            dp.Show();
        }

        private void buatPembelianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuatPembelian bp = new BuatPembelian();
            bp.MdiParent = this;
            bp.Show();
        }

        private void lihatStokToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LihatStok ls = new LihatStok();
            ls.MdiParent = this;
            ls.Show();
        }

        private void lihatPergerakanStokToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PerubahanStok ps = new PerubahanStok();
            ps.MdiParent = this;
            ps.Show();
        }

        private void lihatPembelianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LihatPembelian lp = new LihatPembelian();
            lp.MdiParent = this;
            lp.Show();
        }

        private void buatPenjualanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuatPenjualan bp = new BuatPenjualan();
            bp.MdiParent = this;
            bp.Show();
        }

        private void lihatPenjualanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LihatPenjualan lp = new LihatPenjualan();
            lp.MdiParent = this;
            lp.Show();
        }
    }
}
