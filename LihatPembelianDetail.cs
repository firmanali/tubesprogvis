﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TugasBesarVisualDesktop
{
    public partial class LihatPembelianDetail : Form
    {
        PembelianHeader ph;
        public LihatPembelianDetail(PembelianHeader ph)
        {
            this.ph = ph;
            InitializeComponent();
            setHeader();
            loadItem();
        }

        private void setHeader()
        {
            lblNomor.Text = ph.nomorpembelian;
            lblSupplier.Text = ph.suppliername;
            lblUsername.Text = ph.username;
            lblTanggal.Text = ph.tanggal;
        }
        public void loadItem()
        {
            string sql = "select pd.pembelian_id, p.namaproduk, pd.qty, pd.price, pd.subtotal from pembeliandetail pd, produk p where pd.product_id = p.id and pd.pembelian_id = "+ph.id;
            dataGridView1.DataSource = Connection.getDataTable(sql);
        }
    }
}
