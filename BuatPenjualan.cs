﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TugasBesarVisualDesktop
{
    public partial class BuatPenjualan : Form
    {
        public BuatPenjualan()
        {
            InitializeComponent();
            populateProduk();
        }

        private void populateProduk()
        {
            string sql = "select id, namaproduk from produk";
            cbProduct.ValueMember = "id";
            cbProduct.DisplayMember = "namaproduk";
            cbProduct.DataSource = Connection.getDataTable(sql);
        }

        private void cbProduct_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbProduct.DataSource != null)
            {
                string productId = cbProduct.SelectedValue.ToString();
                string sql = "select saleprice from produk where id = " + productId;
                SqlConnection conn = Connection.koneksi();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = sql;
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    tbHargaJual.Text = String.Format("{0}", reader[0]);
                }
                conn.Close();
            }

        }

        private void btnTambah_Click(object sender, EventArgs e)
        {
            decimal purchaseprice = decimal.Parse(tbHargaJual.Text);
            decimal qty = nmQty.Value;
            decimal disc = nmDiskon.Value;
            decimal subtotal = (purchaseprice-(purchaseprice*disc/100)) * qty;
            string productId = cbProduct.SelectedValue.ToString();
            string namaproduk = cbProduct.Text;
            dataGridView1.Rows.Add(namaproduk, purchaseprice.ToString(), qty.ToString(), disc.ToString(), subtotal.ToString(), productId.ToString());
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex != -1)
            {
                decimal hargajual = decimal.Parse(dataGridView1.Rows[e.RowIndex].Cells["hargajual"].FormattedValue.ToString());
                int qty = int.Parse(dataGridView1.Rows[e.RowIndex].Cells["qty"].FormattedValue.ToString());
                decimal disc = decimal.Parse(dataGridView1.Rows[e.RowIndex].Cells["disc"].FormattedValue.ToString());
                decimal newsubtotal = calculateSubtotal(hargajual,qty,disc);
                dataGridView1.Rows[e.RowIndex].Cells["subtotal"].Value = newsubtotal;
            }
            
        }

        private decimal calculateSubtotal(decimal hargajual, int qty, decimal disc)
        {
            decimal res;
            res = (hargajual - (hargajual * disc / 100)) * qty;
            return res;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            decimal total = 0;
            int x = 0;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                total += decimal.Parse(row.Cells["subtotal"].Value.ToString());
            }
            string sqlcount = "select count(*) from penjualanheader";
            SqlConnection conn = Connection.koneksi();
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = sqlcount;
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                x = int.Parse(String.Format("{0}", reader[0]));
                x++;
            }
            reader.Close();
            DateTime dateTime = DateTime.Now;
            string nomorpenjualan = "PNJ" + dateTime.ToString("yyyyMMdd") + x;
            Console.WriteLine(nomorpenjualan);
            string sql = "insert into penjualanheader values ('" + nomorpenjualan + "','" + dateTime + "'," + Values.userId + "," + total + ")";
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();
            sql = "select id from penjualanheader where nomorpenjualan = '" + nomorpenjualan + "'";
            cmd.CommandText = sql;
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                x = int.Parse(String.Format("{0}", reader[0]));
            }
            reader.Close();
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                decimal price = decimal.Parse(row.Cells["hargajual"].Value.ToString());
                int qty = int.Parse(row.Cells["qty"].Value.ToString());
                decimal disc = decimal.Parse(row.Cells["disc"].Value.ToString());
                decimal subtotal = decimal.Parse(row.Cells["subtotal"].Value.ToString());
                int productId = int.Parse(row.Cells["product_id"].Value.ToString());
                sql = "insert into penjualandetail values (" + x + "," + productId + "," + price + "," + qty + ","+disc+"," + subtotal + ")";
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
                // tambahkan stok
                sql = "select qty from stok where product_id = " + productId;
                cmd.CommandText = sql;
                reader = cmd.ExecuteReader();
                int qtybefore = 0;
                while (reader.Read())
                {
                    qtybefore = int.Parse(String.Format("{0}", reader[0]));
                }
                reader.Close();
                qty = qty * -1;
                int qtyafter = qtybefore + qty;
                sql = "insert into pergerakanstok values ('" + nomorpenjualan + "'," + productId + "," + qtybefore + "," + qty + "," + qtyafter + ", '" + dateTime + "')";
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
                sql = "update stok set qty = " + qtyafter + ", available_qty = " + qtyafter + " where product_id = " + productId;
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
            }
            conn.Close();
            MessageBox.Show("Input Berhasil");
            this.Close();
        }
    }
}
