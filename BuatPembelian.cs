﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TugasBesarVisualDesktop
{
    public partial class BuatPembelian : Form
    {
        string supplierId = "";
        public BuatPembelian()
        {
            InitializeComponent();
        }

        private void BuatPembelian_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'db_projectDataSet2.supplier' table. You can move, or remove it, as needed.
            this.supplierTableAdapter.Fill(this.db_projectDataSet2.supplier);
            initCbSupplier();
        }

        private void initCbSupplier()
        {
            supplierId = cbSupplier.SelectedValue.ToString();
            applySupplierFilter();
        }

        private void cbSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string newsupplierId = cbSupplier.SelectedValue.ToString();
                if (!supplierId.Equals(newsupplierId))
                {
                    dataGridView1.Rows.Clear();
                    dataGridView1.Refresh();
                    supplierId = newsupplierId;
                    if (cbProduct.Items.Count > 0)
                    {
                        cbProduct.DataSource = null;
                        cbProduct.Items.Clear();
                    }
                    applySupplierFilter();
                }
            } catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                this.Close();
            }
           
        }
        private void applySupplierFilter()
        {
            string sql = "select id, namaproduk from produk where supplier_id = '" + supplierId + "'";
            cbProduct.ValueMember = "id";
            cbProduct.DisplayMember = "namaproduk";
            cbProduct.DataSource = Connection.getDataTable(sql);
        }

        private void cbProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            if (cbProduct.DataSource!=null)
            {
                string productId = cbProduct.SelectedValue.ToString();
                string sql = "select purchaseprice from produk where id = " + productId;
                SqlConnection conn = Connection.koneksi();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = sql;
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    tbHargaBeli.Text = String.Format("{0}", reader[0]);
                }
                conn.Close();
            }
            
        }

        private void btnTambah_Click(object sender, EventArgs e)
        {
            decimal purchaseprice = decimal.Parse(tbHargaBeli.Text);
            decimal qty = nmQty.Value;
            decimal subtotal = purchaseprice * qty;
            string productId = cbProduct.SelectedValue.ToString();
            string namaproduk = cbProduct.Text;
            dataGridView1.Rows.Add(namaproduk, purchaseprice.ToString(), qty.ToString(), subtotal.ToString(), productId.ToString());
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            decimal total = 0;
            int x=0;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                total += decimal.Parse(row.Cells["subtotal"].Value.ToString());
            }
            string sqlcount = "select count(*) from pembelianheader";
            SqlConnection conn = Connection.koneksi();
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = sqlcount;
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                x = int.Parse(String.Format("{0}", reader[0]));
                x++;
            }
            reader.Close();
            DateTime dateTime = DateTime.Now;
            string nomorpembelian = "PMB"+dateTime.ToString("yyyyMMdd")+x;
            Console.WriteLine(nomorpembelian);
            string sql = "insert into pembelianheader values ('" + nomorpembelian + "'," + cbSupplier.SelectedValue + "," + total + ", " + Values.userId + ", '"+dateTime+"')";
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();
            sql = "select id from pembelianheader where nomorpembelian = '" + nomorpembelian + "'";
            cmd.CommandText = sql;
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                x = int.Parse(String.Format("{0}", reader[0]));
            }
            reader.Close();
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                decimal price = decimal.Parse(row.Cells["hargabeli"].Value.ToString());
                int qty = int.Parse(row.Cells["qty"].Value.ToString());
                decimal subtotal = decimal.Parse(row.Cells["subtotal"].Value.ToString());
                decimal productId = decimal.Parse(row.Cells["product_id"].Value.ToString());
                sql = "insert into pembeliandetail values (" + x + "," + productId + "," + price + ",0," + qty + "," + subtotal + ")";
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
                // tambahkan stok
                sql = "select qty from stok where product_id = " + productId;
                cmd.CommandText = sql;
                reader = cmd.ExecuteReader();
                int qtybefore = 0;
                while (reader.Read())
                {
                    qtybefore = int.Parse(String.Format("{0}", reader[0]));
                }
                reader.Close();
                int qtyafter = qtybefore + qty;
                sql = "insert into pergerakanstok values ('" + nomorpembelian + "'," + productId + "," + qtybefore + "," + qty + "," + qtyafter + ", '"+dateTime+"')";
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
                sql = "update stok set qty = " + qtyafter + ", available_qty = " + qtyafter + " where product_id = " + productId;
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
            }
            conn.Close();
            MessageBox.Show("Input Berhasil");
            this.Close();
        }
    }
}
