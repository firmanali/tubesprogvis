﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TugasBesarVisualDesktop
{
    public partial class LihatStok : Form
    {
        public LihatStok()
        {
            InitializeComponent();
        }
        public void DaftarStok_Load(object sender, EventArgs e)
        {
            string sql = "select p.id as productid, p.namaproduk, s.id as supplierid, s.namasupplier, st.qty from produk p, supplier s, stok st where p.id = st.product_id and s.id = p.supplier_id";
            dataGridView1.DataSource = Connection.getDataTable(sql);
        }
    }
}
