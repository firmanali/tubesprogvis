﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TugasBesarVisualDesktop
{
    public class Produk
    {
        public Produk(string id, string namaproduk, string kodeproduk, string purchaseprice, string saleprice, string namasupplier, string supplier_id)
        {
            this.id = id;
            this.namaproduk = namaproduk;
            this.kodeproduk = kodeproduk;
            this.purchaseprice = purchaseprice;
            this.saleprice = saleprice;
            this.namasupplier = namasupplier;
            this.supplier_id = supplier_id;
        }

        public string id { get; set; }
        public string namaproduk { get; set; }
        public string kodeproduk { get; set; }
        public string purchaseprice { get; set; }
        public string saleprice { get; set; }
        public string namasupplier { get; set; }
        public string supplier_id { get; set; }
       
    }
}
