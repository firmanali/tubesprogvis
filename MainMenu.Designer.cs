﻿namespace TugasBesarVisualDesktop
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.produkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produkToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.buatProdukToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.daftarProdukToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stokToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lihatStokToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lihatPergerakanStokToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buatSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.daftarSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transaksiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penjualanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buatPenjualanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lihatPenjualanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pembelianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buatPembelianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lihatPembelianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.produkToolStripMenuItem,
            this.transaksiToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(486, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // produkToolStripMenuItem
            // 
            this.produkToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.produkToolStripMenuItem1,
            this.stokToolStripMenuItem,
            this.supplierToolStripMenuItem});
            this.produkToolStripMenuItem.Name = "produkToolStripMenuItem";
            this.produkToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.produkToolStripMenuItem.Text = "Master";
            // 
            // produkToolStripMenuItem1
            // 
            this.produkToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buatProdukToolStripMenuItem,
            this.daftarProdukToolStripMenuItem});
            this.produkToolStripMenuItem1.Name = "produkToolStripMenuItem1";
            this.produkToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.produkToolStripMenuItem1.Text = "Produk";
            // 
            // buatProdukToolStripMenuItem
            // 
            this.buatProdukToolStripMenuItem.Name = "buatProdukToolStripMenuItem";
            this.buatProdukToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.buatProdukToolStripMenuItem.Text = "Buat Produk";
            this.buatProdukToolStripMenuItem.Click += new System.EventHandler(this.buatProdukToolStripMenuItem_Click);
            // 
            // daftarProdukToolStripMenuItem
            // 
            this.daftarProdukToolStripMenuItem.Name = "daftarProdukToolStripMenuItem";
            this.daftarProdukToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.daftarProdukToolStripMenuItem.Text = "Daftar Produk";
            this.daftarProdukToolStripMenuItem.Click += new System.EventHandler(this.daftarProdukToolStripMenuItem_Click);
            // 
            // stokToolStripMenuItem
            // 
            this.stokToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lihatStokToolStripMenuItem,
            this.lihatPergerakanStokToolStripMenuItem});
            this.stokToolStripMenuItem.Name = "stokToolStripMenuItem";
            this.stokToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.stokToolStripMenuItem.Text = "Stok";
            // 
            // lihatStokToolStripMenuItem
            // 
            this.lihatStokToolStripMenuItem.Name = "lihatStokToolStripMenuItem";
            this.lihatStokToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.lihatStokToolStripMenuItem.Text = "Lihat Stok";
            this.lihatStokToolStripMenuItem.Click += new System.EventHandler(this.lihatStokToolStripMenuItem_Click);
            // 
            // lihatPergerakanStokToolStripMenuItem
            // 
            this.lihatPergerakanStokToolStripMenuItem.Name = "lihatPergerakanStokToolStripMenuItem";
            this.lihatPergerakanStokToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.lihatPergerakanStokToolStripMenuItem.Text = "Lihat Pergerakan Stok";
            this.lihatPergerakanStokToolStripMenuItem.Click += new System.EventHandler(this.lihatPergerakanStokToolStripMenuItem_Click);
            // 
            // supplierToolStripMenuItem
            // 
            this.supplierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buatSupplierToolStripMenuItem,
            this.daftarSupplierToolStripMenuItem});
            this.supplierToolStripMenuItem.Name = "supplierToolStripMenuItem";
            this.supplierToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.supplierToolStripMenuItem.Text = "Supplier";
            // 
            // buatSupplierToolStripMenuItem
            // 
            this.buatSupplierToolStripMenuItem.Name = "buatSupplierToolStripMenuItem";
            this.buatSupplierToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.buatSupplierToolStripMenuItem.Text = "Buat Supplier";
            this.buatSupplierToolStripMenuItem.Click += new System.EventHandler(this.buatSupplierToolStripMenuItem_Click);
            // 
            // daftarSupplierToolStripMenuItem
            // 
            this.daftarSupplierToolStripMenuItem.Name = "daftarSupplierToolStripMenuItem";
            this.daftarSupplierToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.daftarSupplierToolStripMenuItem.Text = "Daftar Supplier";
            this.daftarSupplierToolStripMenuItem.Click += new System.EventHandler(this.daftarSupplierToolStripMenuItem_Click);
            // 
            // transaksiToolStripMenuItem
            // 
            this.transaksiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.penjualanToolStripMenuItem,
            this.pembelianToolStripMenuItem});
            this.transaksiToolStripMenuItem.Name = "transaksiToolStripMenuItem";
            this.transaksiToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.transaksiToolStripMenuItem.Text = "Transaksi";
            // 
            // penjualanToolStripMenuItem
            // 
            this.penjualanToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buatPenjualanToolStripMenuItem,
            this.lihatPenjualanToolStripMenuItem});
            this.penjualanToolStripMenuItem.Name = "penjualanToolStripMenuItem";
            this.penjualanToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.penjualanToolStripMenuItem.Text = "Penjualan";
            // 
            // buatPenjualanToolStripMenuItem
            // 
            this.buatPenjualanToolStripMenuItem.Name = "buatPenjualanToolStripMenuItem";
            this.buatPenjualanToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.buatPenjualanToolStripMenuItem.Text = "Buat Penjualan";
            this.buatPenjualanToolStripMenuItem.Click += new System.EventHandler(this.buatPenjualanToolStripMenuItem_Click);
            // 
            // lihatPenjualanToolStripMenuItem
            // 
            this.lihatPenjualanToolStripMenuItem.Name = "lihatPenjualanToolStripMenuItem";
            this.lihatPenjualanToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.lihatPenjualanToolStripMenuItem.Text = "Lihat Penjualan";
            this.lihatPenjualanToolStripMenuItem.Click += new System.EventHandler(this.lihatPenjualanToolStripMenuItem_Click);
            // 
            // pembelianToolStripMenuItem
            // 
            this.pembelianToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buatPembelianToolStripMenuItem,
            this.lihatPembelianToolStripMenuItem});
            this.pembelianToolStripMenuItem.Name = "pembelianToolStripMenuItem";
            this.pembelianToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.pembelianToolStripMenuItem.Text = "Pembelian";
            // 
            // buatPembelianToolStripMenuItem
            // 
            this.buatPembelianToolStripMenuItem.Name = "buatPembelianToolStripMenuItem";
            this.buatPembelianToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.buatPembelianToolStripMenuItem.Text = "Buat Pembelian";
            this.buatPembelianToolStripMenuItem.Click += new System.EventHandler(this.buatPembelianToolStripMenuItem_Click);
            // 
            // lihatPembelianToolStripMenuItem
            // 
            this.lihatPembelianToolStripMenuItem.Name = "lihatPembelianToolStripMenuItem";
            this.lihatPembelianToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.lihatPembelianToolStripMenuItem.Text = "Lihat Pembelian";
            this.lihatPembelianToolStripMenuItem.Click += new System.EventHandler(this.lihatPembelianToolStripMenuItem_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 261);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainMenu";
            this.Text = "MainMenu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainMenu_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem produkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produkToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem buatProdukToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem daftarProdukToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stokToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lihatStokToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lihatPergerakanStokToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buatSupplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem daftarSupplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transaksiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penjualanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buatPenjualanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lihatPenjualanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pembelianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buatPembelianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lihatPembelianToolStripMenuItem;
    }
}