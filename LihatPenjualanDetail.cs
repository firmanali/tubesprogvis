﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TugasBesarVisualDesktop
{
    public partial class LihatPenjualanDetail : Form
    {
        PenjualanHeader ph;
        public LihatPenjualanDetail(PenjualanHeader ph)
        {
            InitializeComponent();
            this.ph = ph;
            setHeader();
            loadItem();
        }
        private void setHeader()
        {
            lblNomor.Text = ph.nomorpenjualan;
            lblUsername.Text = ph.username;
            lblTanggal.Text = ph.tanggal;
        }
        public void loadItem()
        {
            string sql = "select pd.penjualan_id, p.namaproduk, pd.qty, pd.price, pd.discountpct as disc, pd.subtotal from penjualandetail pd, produk p where pd.product_id = p.id and pd.penjualan_id = " + ph.id;
            dataGridView1.DataSource = Connection.getDataTable(sql);
        }
    }
}
