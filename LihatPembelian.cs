﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TugasBesarVisualDesktop
{
    public partial class LihatPembelian : Form
    {
        public LihatPembelian()
        {
            InitializeComponent();
        }

        public void DaftarPembelian_Load(object sender, EventArgs e)
        {
            string sql = "select p.id, p.nomorpembelian, p.supplier_id, p.total, p.tanggal, s.namasupplier, u.username, p.user_id from pembelianheader p, supplier s, users u where p.supplier_id = s.id and p.user_id = u.id";
            dataGridView1.DataSource = Connection.getDataTable(sql);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string id = dataGridView1.Rows[e.RowIndex].Cells["id"].FormattedValue.ToString();
            string nomorpembelian = dataGridView1.Rows[e.RowIndex].Cells["nomorpembelian"].FormattedValue.ToString();
            string namasupplier = dataGridView1.Rows[e.RowIndex].Cells["supplier"].FormattedValue.ToString();
            string tanggal = dataGridView1.Rows[e.RowIndex].Cells["tanggal"].FormattedValue.ToString();
            string total = dataGridView1.Rows[e.RowIndex].Cells["total"].FormattedValue.ToString();
            string username = dataGridView1.Rows[e.RowIndex].Cells["user"].FormattedValue.ToString();
            string supplier_id = dataGridView1.Rows[e.RowIndex].Cells["supplier_id"].FormattedValue.ToString();
            string user_id = dataGridView1.Rows[e.RowIndex].Cells["user_id"].FormattedValue.ToString();
            PembelianHeader ph = new PembelianHeader(id,supplier_id,user_id,nomorpembelian,namasupplier,username,tanggal,total);
            LihatPembelianDetail lpd = new LihatPembelianDetail(ph);
            lpd.MdiParent = this.MdiParent;
            lpd.Show();
        }
    }
}
