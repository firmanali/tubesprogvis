﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TugasBesarVisualDesktop
{
    public partial class DaftarSupplier : Form
    {
        public DaftarSupplier()
        {
            InitializeComponent();
        }

        private void DaftarSupplier_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'db_projectDataSet1.supplier' table. You can move, or remove it, as needed.
            this.supplierTableAdapter.Fill(this.db_projectDataSet1.supplier);
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string id = dataGridView1.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
            string nama = dataGridView1.Rows[e.RowIndex].Cells[1].FormattedValue.ToString();
            string alamat = dataGridView1.Rows[e.RowIndex].Cells[2].FormattedValue.ToString();
            DetailSupplier ds = new DetailSupplier();
            ds.MdiParent = this.MdiParent;
            ds.initData(id, nama, alamat);
            ds.Show();
        }

        public void loadData()
        {
            string sql = "select id, namasupplier, alamatsupplier from supplier";
            dataGridView1.DataSource = Connection.getDataTable(sql);
        }
    }
}
