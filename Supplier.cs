﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TugasBesarVisualDesktop
{
    class Supplier
    {
        public Supplier(string id, string namasupplier, string alamatsupplier)
        {
            this.id = id;
            this.namasupplier = namasupplier;
            this.alamatsupplier = alamatsupplier;
        }
        public string id { get; set; }
        public string namasupplier { get; set; }
        public string alamatsupplier { get; set; }

    }
    

}
