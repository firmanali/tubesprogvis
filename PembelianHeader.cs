﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TugasBesarVisualDesktop
{
    public class PembelianHeader
    {
        public string id { get; set; }
        public string supplier_id { get; set; }
        public string user_id { get; set; }
        public string nomorpembelian { get; set; }
        public string suppliername { get; set; }
        public string username { get; set; }
        public string tanggal { get; set; }
        public string total { get; set; }
        public PembelianHeader(string id, string supplier_id, string user_id, string nomorpembelian, string suppliername, string username, string tanggal, string total)
        {
            this.id = id;
            this.supplier_id = supplier_id;
            this.user_id = user_id;
            this.nomorpembelian = nomorpembelian;
            this.suppliername = suppliername;
            this.username = username;
            this.tanggal = tanggal;
            this.total = total;
        }
    }
}
