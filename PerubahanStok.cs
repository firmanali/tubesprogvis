﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TugasBesarVisualDesktop
{
    public partial class PerubahanStok : Form
    {
        public PerubahanStok()
        {
            InitializeComponent();
        }
        public void PerubahanStok_Load(object sender, EventArgs e)
        {
            string sql = "select ps.nomortransaksi, p.namaproduk, ps.qtysebelum, ps.qtyperubahan, ps.qtysesudah, ps.tanggal from pergerakanstok ps, produk p where ps.product_id = p.id;";
            dataGridView1.DataSource = Connection.getDataTable(sql);
        }
    }
}
