﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TugasBesarVisualDesktop
{
    public partial class DetailProduk : Form
    {
        Produk p;
        DaftarProduk dp;
        public DetailProduk(Produk p)
        {
            InitializeComponent();
            cbSupplierLoad();
            this.p = p;
            isiData(p);
        }
        private void isiData(Produk p)
        {
            string id = p.id;
            tbNama.Text = p.namaproduk;
            tbKode.Text = p.kodeproduk;
            tbHargaBeli.Value = decimal.Parse(p.purchaseprice);
            tbHargaJual.Value = decimal.Parse(p.saleprice);
            cbSupplier.SelectedValue = p.supplier_id;
            dp = (DaftarProduk)Application.OpenForms["DaftarProduk"];
        }
        private void cbSupplierLoad()
        {
            string sql = "select id, namasupplier from supplier";
            cbSupplier.ValueMember = "id";
            cbSupplier.DisplayMember = "namasupplier";
            cbSupplier.DataSource = Connection.getDataTable(sql);
        }

        private void btnUbah_Click(object sender, EventArgs e)
        {
            string namaprodukbaru = tbNama.Text;
            string kodeprodukbaru = tbKode.Text;
            string hargabelibaru = tbHargaBeli.Value.ToString();
            string hargajualbaru = tbHargaJual.Value.ToString();
            string supplierbaru = cbSupplier.SelectedValue.ToString();
            string sql = "update produk set namaproduk = '" + namaprodukbaru + "', kodeproduk = '" + kodeprodukbaru + "', purchaseprice = " + hargabelibaru + ", saleprice = " + hargajualbaru + ", supplier_id = " + supplierbaru+" where id = "+p.id;
            int x = Connection.executeNonQuery(sql);
            if (x > 0)
            {
                MessageBox.Show("Update Berhasil");
                dp.DaftarProduct_Load(sender, e);
                dp.dataGridView1.Update();
                dp.dataGridView1.Refresh();
                this.Close();
            }
            else
            {
                MessageBox.Show("Gagal Update");
            }
        }

        private void btnHapus_Click(object sender, EventArgs e)
        {
            string sql = "delete from produk where id = "+p.id;
            int x = Connection.executeNonQuery(sql);
            if (x > 0)
            {
                MessageBox.Show("Data berhasil dihapus");
                dp.DaftarProduct_Load(sender, e);
                dp.dataGridView1.Update();
                dp.dataGridView1.Refresh();
                this.Close();
            }
            else
            {
                MessageBox.Show("Gagal menghapus data");
            }
        }
    }
}
