﻿namespace TugasBesarVisualDesktop
{
    partial class PerubahanStok
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.nomortransaksi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namaproduk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtysebelum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtyperubahan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtysesudah = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tanggal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nomortransaksi,
            this.namaproduk,
            this.qtysebelum,
            this.qtyperubahan,
            this.qtysesudah,
            this.tanggal});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(644, 261);
            this.dataGridView1.TabIndex = 0;
            // 
            // nomortransaksi
            // 
            this.nomortransaksi.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nomortransaksi.DataPropertyName = "nomortransaksi";
            this.nomortransaksi.HeaderText = "Nomor Transaksi";
            this.nomortransaksi.Name = "nomortransaksi";
            this.nomortransaksi.ReadOnly = true;
            // 
            // namaproduk
            // 
            this.namaproduk.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.namaproduk.DataPropertyName = "namaproduk";
            this.namaproduk.HeaderText = "Nama Produk";
            this.namaproduk.Name = "namaproduk";
            this.namaproduk.ReadOnly = true;
            // 
            // qtysebelum
            // 
            this.qtysebelum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.qtysebelum.DataPropertyName = "qtysebelum";
            this.qtysebelum.HeaderText = "Qty Sebelum";
            this.qtysebelum.Name = "qtysebelum";
            this.qtysebelum.ReadOnly = true;
            // 
            // qtyperubahan
            // 
            this.qtyperubahan.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.qtyperubahan.DataPropertyName = "qtyperubahan";
            this.qtyperubahan.HeaderText = "Qty Perubahan";
            this.qtyperubahan.Name = "qtyperubahan";
            this.qtyperubahan.ReadOnly = true;
            // 
            // qtysesudah
            // 
            this.qtysesudah.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.qtysesudah.DataPropertyName = "qtysesudah";
            this.qtysesudah.HeaderText = "Qty Sesudah";
            this.qtysesudah.Name = "qtysesudah";
            this.qtysesudah.ReadOnly = true;
            // 
            // tanggal
            // 
            this.tanggal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tanggal.DataPropertyName = "tanggal";
            this.tanggal.HeaderText = "Tanggal";
            this.tanggal.Name = "tanggal";
            this.tanggal.ReadOnly = true;
            // 
            // PerubahanStok
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 261);
            this.Controls.Add(this.dataGridView1);
            this.Name = "PerubahanStok";
            this.Text = "PerubahanStok";
            this.Load += new System.EventHandler(this.PerubahanStok_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomortransaksi;
        private System.Windows.Forms.DataGridViewTextBoxColumn namaproduk;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtysebelum;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtyperubahan;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtysesudah;
        private System.Windows.Forms.DataGridViewTextBoxColumn tanggal;
    }
}