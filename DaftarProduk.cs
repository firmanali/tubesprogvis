﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TugasBesarVisualDesktop
{
    public partial class DaftarProduk : Form
    {
        public DaftarProduk()
        {
            InitializeComponent();
        }
        public void DaftarProduct_Load(object sender, EventArgs e)
        {
            string sql = "select p.id, p.namaproduk, p.kodeproduk, p.purchaseprice, p.saleprice, p.supplier_id, s.namasupplier from produk p, supplier s where p.supplier_id = s.id";
            dataGridView1.DataSource = Connection.getDataTable(sql);
        }

        private void actionDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string id = dataGridView1.Rows[e.RowIndex].Cells["id"].FormattedValue.ToString();
            string namaproduk = dataGridView1.Rows[e.RowIndex].Cells["namaproduk"].FormattedValue.ToString();
            string kodeproduk = dataGridView1.Rows[e.RowIndex].Cells["kodeproduk"].FormattedValue.ToString();
            string hargabeli = dataGridView1.Rows[e.RowIndex].Cells["purchaseprice"].FormattedValue.ToString();
            string hargajual = dataGridView1.Rows[e.RowIndex].Cells["saleprice"].FormattedValue.ToString();
            string supplier_id = dataGridView1.Rows[e.RowIndex].Cells["supplier_id"].FormattedValue.ToString();
            string namasupplier = dataGridView1.Rows[e.RowIndex].Cells["namasupplier"].FormattedValue.ToString();
            Produk p = new Produk(id, namaproduk, kodeproduk, hargabeli, hargajual, namasupplier, supplier_id);
            DetailProduk dp = new DetailProduk(p);
            dp.MdiParent = this.MdiParent;
            dp.Show();
        }
    }
}
