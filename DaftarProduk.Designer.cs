﻿namespace TugasBesarVisualDesktop
{
    partial class DaftarProduk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.namaproduk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kodeproduk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purchaseprice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saleprice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namasupplier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplier_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.namaproduk,
            this.kodeproduk,
            this.purchaseprice,
            this.saleprice,
            this.namasupplier,
            this.id,
            this.supplier_id});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(508, 261);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.actionDoubleClick);
            // 
            // namaproduk
            // 
            this.namaproduk.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.namaproduk.DataPropertyName = "namaproduk";
            this.namaproduk.HeaderText = "Nama Produk";
            this.namaproduk.Name = "namaproduk";
            this.namaproduk.ReadOnly = true;
            // 
            // kodeproduk
            // 
            this.kodeproduk.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.kodeproduk.DataPropertyName = "kodeproduk";
            this.kodeproduk.HeaderText = "Kode Produk";
            this.kodeproduk.Name = "kodeproduk";
            this.kodeproduk.ReadOnly = true;
            // 
            // purchaseprice
            // 
            this.purchaseprice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.purchaseprice.DataPropertyName = "purchaseprice";
            this.purchaseprice.HeaderText = "Harga Beli";
            this.purchaseprice.Name = "purchaseprice";
            this.purchaseprice.ReadOnly = true;
            // 
            // saleprice
            // 
            this.saleprice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.saleprice.DataPropertyName = "saleprice";
            this.saleprice.HeaderText = "Harga Jual";
            this.saleprice.Name = "saleprice";
            this.saleprice.ReadOnly = true;
            // 
            // namasupplier
            // 
            this.namasupplier.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.namasupplier.DataPropertyName = "namasupplier";
            this.namasupplier.HeaderText = "Supplier";
            this.namasupplier.Name = "namasupplier";
            this.namasupplier.ReadOnly = true;
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // supplier_id
            // 
            this.supplier_id.DataPropertyName = "supplier_id";
            this.supplier_id.HeaderText = "supplier_id";
            this.supplier_id.Name = "supplier_id";
            this.supplier_id.ReadOnly = true;
            this.supplier_id.Visible = false;
            // 
            // DaftarProduk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 261);
            this.Controls.Add(this.dataGridView1);
            this.Name = "DaftarProduk";
            this.Text = "DaftarProduk";
            this.Load += new System.EventHandler(this.DaftarProduct_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridViewTextBoxColumn namaproduk;
        private System.Windows.Forms.DataGridViewTextBoxColumn kodeproduk;
        private System.Windows.Forms.DataGridViewTextBoxColumn purchaseprice;
        private System.Windows.Forms.DataGridViewTextBoxColumn saleprice;
        private System.Windows.Forms.DataGridViewTextBoxColumn namasupplier;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplier_id;
        public System.Windows.Forms.DataGridView dataGridView1;
    }
}