﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TugasBesarVisualDesktop
{
    public partial class LihatPenjualan : Form
    {
        public LihatPenjualan()
        {
            InitializeComponent();
        }
        public void DaftarPenjualan_Load(object sender, EventArgs e)
        {
            string sql = "select p.id, p.nomorpenjualan, p.total, p.tanggal, u.username, p.user_id from penjualanheader p, users u where p.user_id = u.id";
            dataGridView1.DataSource = Connection.getDataTable(sql);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string id = dataGridView1.Rows[e.RowIndex].Cells["id"].FormattedValue.ToString();
            string nomorpenjualan = dataGridView1.Rows[e.RowIndex].Cells["nomorpenjualan"].FormattedValue.ToString();
            
            string tanggal = dataGridView1.Rows[e.RowIndex].Cells["tanggal"].FormattedValue.ToString();
            string total = dataGridView1.Rows[e.RowIndex].Cells["total"].FormattedValue.ToString();
            string username = dataGridView1.Rows[e.RowIndex].Cells["user"].FormattedValue.ToString();
            
            string user_id = dataGridView1.Rows[e.RowIndex].Cells["user_id"].FormattedValue.ToString();
            PenjualanHeader ph = new PenjualanHeader(id,user_id, nomorpenjualan, username, tanggal, total);
            LihatPenjualanDetail lpd = new LihatPenjualanDetail(ph);
            lpd.MdiParent = this.MdiParent;
            lpd.Show();
        }
    }
}
