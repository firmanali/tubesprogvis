﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace TugasBesarVisualDesktop
{
    class Connection
    {
        public static SqlConnection koneksi()
        {
            string connection_string = @"Data Source=DESKTOP-M29G9PM\SQLEXPRESS;Initial Catalog=db_project;Integrated Security=True";
            SqlConnection conn = new SqlConnection(connection_string);
            return conn;
        }

        public static int executeNonQuery(string sql)
        {
            SqlConnection conn = Connection.koneksi();
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = sql;
            int x = cmd.ExecuteNonQuery();
            conn.Close();
            return x;
        }

        public static SqlDataReader executeQuery(string sql)
        {
            SqlConnection conn = Connection.koneksi();
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = sql;
            SqlDataReader reader = cmd.ExecuteReader();
            conn.Close();
            return reader;
        }

        public static DataTable getDataTable(string sql)
        {
            SqlConnection conn = Connection.koneksi();
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conn.Close();
            return dt;
        }
    }
}
