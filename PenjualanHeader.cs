﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TugasBesarVisualDesktop
{
    public class PenjualanHeader
    {
        public string id { get; set; }
        
        public string user_id { get; set; }
        public string nomorpenjualan { get; set; }
        
        public string username { get; set; }
        public string tanggal { get; set; }
        public string total { get; set; }
        public PenjualanHeader(string id, string user_id, string nomorpembelian, string username, string tanggal, string total)
        {
            this.id = id;
            this.user_id = user_id;
            this.nomorpenjualan = nomorpembelian;
            this.username = username;
            this.tanggal = tanggal;
            this.total = total;
        }
    }
}
