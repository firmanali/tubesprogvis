﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TugasBesarVisualDesktop
{
    public partial class BuatProduk : Form
    {
        public BuatProduk()
        {
            InitializeComponent();
            this.cbSupplierLoad();
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            string nama = tbNama.Text;
            string kode = tbKode.Text;
            string hargaBeli = tbHargaBeli.Text;
            string hargaJual = tbHargaJual.Text;
            string supplierId = cbSupplier.SelectedValue.ToString();
            string sql = "insert into produk values ('"+nama+"','"+kode+"',"+supplierId+","+hargaBeli+","+hargaJual+")";
            int x = Connection.executeNonQuery(sql);
            if(x > 0)
            {
                int id = 0;
                //buat record stoknya
                sql = "select id from produk where namaproduk = '" + nama + "'";
                SqlConnection conn = Connection.koneksi();
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = sql;
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    id = int.Parse(String.Format("{0}", reader[0]));
                }
                reader.Close();
                sql = "insert into stok values (" + id + ",0,0)";
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Produk berhasil ditambahkan");
                tbNama.Text = "";
                tbKode.Text = "";
                tbHargaBeli.Text = "0";
                tbHargaJual.Text = "0";
            } else
            {
                MessageBox.Show("Gagal menambah produk");
            }
        }

        private void cbSupplierLoad()
        {
            string sql = "select id, namasupplier from supplier";
            cbSupplier.ValueMember = "id";
            cbSupplier.DisplayMember = "namasupplier";
            cbSupplier.DataSource = Connection.getDataTable(sql);
        }
    }
}
